package example.person.adapter.persistence;

import com.gs.fw.finder.Operation;

import java.util.Collection;

class PersonList extends PersonEntityListAbstract
{
	public PersonList()
	{
		super();
	}

	public PersonList(int initialSize)
	{
		super(initialSize);
	}

	public PersonList(Collection c)
	{
		super(c);
	}

	public PersonList(Operation operation)
	{
		super(operation);
	}
}
