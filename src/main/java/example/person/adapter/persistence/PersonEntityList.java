package example.person.adapter.persistence;
import com.gs.fw.finder.Operation;
import java.util.*;
class PersonEntityList extends PersonEntityListAbstract
{
	public PersonEntityList()
	{
		super();
	}

	public PersonEntityList(int initialSize)
	{
		super(initialSize);
	}

	public PersonEntityList(Collection c)
	{
		super(c);
	}

	public PersonEntityList(Operation operation)
	{
		super(operation);
	}
}
