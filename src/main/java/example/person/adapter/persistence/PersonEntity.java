package example.person.adapter.persistence;
import com.gs.fw.common.mithra.finder.Operation;
import com.gs.fw.common.mithra.util.DefaultInfinityTimestamp;

import java.sql.Timestamp;

public class PersonEntity extends PersonEntityAbstract {
	public PersonEntity(Timestamp ts)
	{
		super(ts);
		// You must not modify this constructor. Mithra calls this internally.
		// You can call this constructor. You can also add new constructors.
	}

	public String getFullName()
	{
		return this.getFirstName() + " " + getLastName();
	}

	public static void createPerson(String firstName, String lastName, String country)
	{
		PersonEntity person = new PersonEntity(DefaultInfinityTimestamp.getDefaultInfinity());
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setCountry(country);

		person.insert();
	}

	public static PersonEntity findPersonNamed(String firstName, String lastName)
	{
		Operation findByFirstNameAndLastName =
				PersonEntityFinder.firstName().eq(firstName)
						.and(PersonEntityFinder.lastName().eq(lastName));
		return PersonEntityFinder.findOne(findByFirstNameAndLastName);
	}
}
