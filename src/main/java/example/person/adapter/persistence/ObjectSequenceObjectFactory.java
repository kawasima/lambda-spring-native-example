package example.person.adapter.persistence;

import com.gs.fw.common.mithra.MithraSequence;
import com.gs.fw.common.mithra.MithraSequenceObjectFactory;
import example.person.adapter.persistence.ObjectSequence;
import example.person.adapter.persistence.ObjectSequenceFinder;

public class ObjectSequenceObjectFactory implements MithraSequenceObjectFactory {
    public MithraSequence getMithraSequenceObject(String sequenceName, Object sourceAttribute, int initialValue)
    {
        ObjectSequence objectSequence = ObjectSequenceFinder.findByPrimaryKey(sequenceName);
        if (objectSequence == null)
        {
            objectSequence = new ObjectSequence();
            objectSequence.setSequenceName(sequenceName);
            objectSequence.setNextId(initialValue);
            objectSequence.insert();
        }
        return objectSequence;
    }
}
