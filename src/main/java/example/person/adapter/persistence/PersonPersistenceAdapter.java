package example.person.adapter.persistence;

import example.person.application.SavePersonPort;
import example.person.domain.Person;
import org.springframework.stereotype.Component;

@Component
class PersonPersistenceAdapter implements SavePersonPort {
    @Override
    public void save(Person person) {
        PersonEntity.createPerson(
                person.getFirstName().getValue(),
                person.getLastName().getValue(),
                person.getCountry().getValue()
        );
    }
}
