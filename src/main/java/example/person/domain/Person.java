package example.person.domain;

import am.ik.yavi.arguments.Arguments1;
import am.ik.yavi.arguments.Arguments2;
import am.ik.yavi.arguments.Arguments3;
import am.ik.yavi.arguments.Arguments3Validator;
import am.ik.yavi.builder.ArgumentsValidatorBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Person {
    public static final Arguments3Validator<FirstName, LastName, Country, Person> validator = ArgumentsValidatorBuilder.of(Person::new)
            .builder(b -> b._object(Arguments1::arg1, "firstName", c -> c.notNull()))
            .builder(b -> b._object(Arguments2::arg2, "lastName", c -> c.notNull()))
            .builder(b -> b._object(Arguments3::arg3, "country", c -> c.notNull()))
            .build();

    FirstName firstName;
    LastName lastName;
    Country country;

    public static Person of(FirstName firstName, LastName lastName, Country country) {
        return validator.validated(firstName, lastName, country);
    }
}
