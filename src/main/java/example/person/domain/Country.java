package example.person.domain;

import am.ik.yavi.arguments.StringValidator;
import am.ik.yavi.builder.StringValidatorBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Country {
    public static final StringValidator<Country> validator = StringValidatorBuilder
            .of("value", c -> c.notBlank().lessThanOrEqual(2))
            .build()
            .andThen(Country::new);

    String value;

    public static Country of(String value) {
        return validator.validated(value);
    }
    @Override
    public String toString() {
        return value;
    }
}
