package example.person.domain;

import am.ik.yavi.arguments.StringValidator;
import am.ik.yavi.builder.StringValidatorBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FirstName {
    public static final StringValidator<FirstName> validator = StringValidatorBuilder
            .of("value", c -> c.notBlank().lessThanOrEqual(50))
            .build()
            .andThen(FirstName::new);

    String value;

    public static FirstName of(String value) {
        return validator.validated(value);
    }
    @Override
    public String toString() {
        return value;
    }
}
