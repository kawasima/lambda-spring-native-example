package example.person.application;

public interface PersonSaveUseCase {
    SavedPersonEvent handle(PersonSaveCommand command);
}
