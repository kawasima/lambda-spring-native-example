package example.person.application;

import lombok.Value;

@Value
public class PersonSaveCommand {
    String firstName;
    String lastName;
    String country;
}
