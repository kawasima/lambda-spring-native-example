package example.person.application.impl;

import com.gs.fw.common.mithra.MithraManagerProvider;
import example.person.application.PersonSaveCommand;
import example.person.application.PersonSaveUseCase;
import example.person.application.SavePersonPort;
import example.person.application.SavedPersonEvent;
import example.person.domain.Country;
import example.person.domain.FirstName;
import example.person.domain.LastName;
import example.person.domain.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonSaveUseCaseImpl implements PersonSaveUseCase {
    private final SavePersonPort savePersonPort;

    public PersonSaveUseCaseImpl(SavePersonPort savePersonPort) {
        this.savePersonPort = savePersonPort;
    }

    @Override
    public SavedPersonEvent handle(PersonSaveCommand command) {
        Person person = Person.of(
                FirstName.of(command.getFirstName()),
                LastName.of(command.getLastName()),
                Country.of(command.getCountry()));
        return MithraManagerProvider.getMithraManager().executeTransactionalCommand(tx -> {
            savePersonPort.save(person);
            return new SavedPersonEvent();
        });
    }
}
