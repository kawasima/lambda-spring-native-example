package example.person.application;

import example.person.domain.Person;

public interface SavePersonPort {
    void save(Person person);
}
