package example.function;

import example.person.application.PersonSaveCommand;
import example.person.application.PersonSaveUseCase;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@Component("savePerson")
public class SavePersonFunction implements Function<Mono<Map<String, Object>>, Mono<Map<String, Object>>> {
    private final PersonSaveUseCase personSaveUseCase;

    public SavePersonFunction(PersonSaveUseCase personSaveUseCase) {
        this.personSaveUseCase = personSaveUseCase;
    }

    @Override
    public Mono<Map<String, Object>> apply(Mono<Map<String, Object>> request) {
        return request.map(m -> {
            PersonSaveCommand command = new PersonSaveCommand(
                    Objects.toString(m.get("firstName"), null),
                    Objects.toString(m.get("lastName"), null),
                    Objects.toString(m.get("country"), null));
            personSaveUseCase.handle(command);

            return m;
        });
    }
}
