package example.config;

import com.gs.fw.common.mithra.MithraManager;
import com.gs.fw.common.mithra.MithraManagerProvider;
import example.util.H2ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
public class ReladomoSetup implements InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(ReladomoSetup.class.getName());
    private static final int MAX_TRANSACTION_TIMEOUT = 120;
    @Override
    public void afterPropertiesSet() throws Exception {
        H2ConnectionManager.getInstance().prepareTables();

        initialiseReladomo();
        loadReladomoConfigurationXml("reladomo/config/ReladomoRuntimeConfig.xml");
    }

    private void initialiseReladomo() throws Exception
    {
        try
        {
            logger.info("Transaction Timeout is " + MAX_TRANSACTION_TIMEOUT);
            MithraManager mithraManager = MithraManagerProvider.getMithraManager();
            mithraManager.setTransactionTimeout(MAX_TRANSACTION_TIMEOUT);
            // Notification should be configured here. Refer to notification/Notification.html under reladomo-javadoc.jar.
        }
        catch (Exception e)
        {
            logger.error("Unable to initialise Reladomo!", e);
            throw new Exception("Unable to initialise Reladomo!", e);
        }
        logger.info("Reladomo has been initialised!");
    }

    private void loadReladomoConfigurationXml(String reladomoRuntimeConfig) throws Exception
    {
        logger.info("Reladomo configuration XML is " + reladomoRuntimeConfig);
        InputStream is = ReladomoSetup.class.getClassLoader().getResourceAsStream(reladomoRuntimeConfig);
        if (is == null) throw new Exception("can't find file: " + reladomoRuntimeConfig + " in classpath");
        MithraManagerProvider.getMithraManager().readConfiguration(is);
        try
        {
            is.close();
        }
        catch (java.io.IOException e)
        {
            logger.error("Unable to initialise Reladomo!", e);
            throw new Exception("Unable to initialise Reladomo!", e);
        }
        logger.info("Reladomo configuration XML " + reladomoRuntimeConfig+" is now loaded.");
    }
}
